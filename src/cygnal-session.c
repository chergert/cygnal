/* cygnal-session.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "cygnal-config.h"

#define G_LOG_DOMAIN "cygnal-session"

#include <gcrypt.h>
#include <signal/signal_protocol.h>

#include "cygnal-session.h"

#define SHA256_DIGEST_LEN 32
#define SHA512_DIGEST_LEN 64

struct _CygnalSession
{
  GObject         parent;
  GRecMutex       context_lock;
  signal_context *context;
};

static void async_initable_iface_init (GAsyncInitableIface *iface);

G_DEFINE_TYPE_WITH_CODE (CygnalSession, cygnal_session, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE,
                                                async_initable_iface_init))

enum {
  PROP_0,
  N_PROPS
};

/**
 * cygnal_session_new:
 *
 * Create a new #CygnalSession.
 *
 * Returns: (transfer full): a newly created #CygnalSession
 * 
 * Since: 3.32
 */
CygnalSession *
cygnal_session_new (void)
{
  return g_object_new (CYGNAL_TYPE_SESSION, NULL);
}

static inline gsize
next_power_of_two (gsize v)
{
   v--;
   v |= v >> 1;
   v |= v >> 2;
   v |= v >> 4;
   v |= v >> 8;
   v |= v >> 16;
#if GLIB_SIZEOF_SIZE_T == 4
#elif GLIB_SIZEOF_SIZE_T == 8
   v |= v >> 32;
#else
# error "Unsupported size for size_t"
#endif
   v++;
   return v;
}

static void
cygnal_session_lock_cb (gpointer user_data)
{
  CygnalSession *self = user_data;
  g_assert (CYGNAL_IS_SESSION (self));
  g_rec_mutex_lock (&self->context_lock);
}

static void
cygnal_session_unlock_cb (gpointer user_data)
{
  CygnalSession *self = user_data;
  g_assert (CYGNAL_IS_SESSION (self));
  g_rec_mutex_unlock (&self->context_lock);
}

static void
cygnal_session_log_cb (int         level,
                       const char *message,
                       size_t      len,
                       void       *user_data)
{
    switch (level) {
    case SG_LOG_ERROR:
      g_error ("%s", message);
      break;

    case SG_LOG_WARNING:
      g_warning ("%s", message);
      break;

    case SG_LOG_INFO:
      g_info ("%s", message);
      break;

    case SG_LOG_DEBUG:
      g_debug ("%s", message);
      break;

    case SG_LOG_NOTICE:
    default:
      g_message ("%s", message);
      break;
    }
}

static gint
cygnal_session_random_generator_cb (guint8   *data,
                                    gsize     len,
                                    gpointer  user_data)
{
  g_assert (CYGNAL_IS_SESSION (user_data));

  gcry_create_nonce (data, len);

  return 0;
}

static gint
cygnal_session_hmac_sha256_init_cb (gpointer     *hmac_context,
                                    const guint8 *key,
                                    gsize         key_len,
                                    gpointer      user_data)
{
  CygnalSession *self = user_data;
  gcry_md_hd_t *handle;
  gcry_error_t err;

  g_assert (CYGNAL_IS_SESSION (self));

  handle = g_slice_new0 (gcry_md_hd_t);

  err = gcry_md_open (handle, GCRY_MD_SHA256, GCRY_MD_FLAG_SECURE);

  if (err != GPG_ERR_NO_ERROR)
    goto failure;

  err = gcry_md_setkey (*handle, key, key_len);

  if (err != GPG_ERR_NO_ERROR)
    {
      gcry_md_close (*handle);
      goto failure;
    }

  *hmac_context = g_steal_pointer (&handle);

  return 0;

failure:
  g_slice_free (gcry_md_hd_t, handle);
  *hmac_context = NULL;

  return SG_ERR_NOMEM;
}

static gint
cygnal_session_hmac_sha256_update_cb (gpointer      hmac_context,
                                      const guint8 *data,
                                      gsize         data_len,
                                      gpointer      user_data)
{
  gcry_md_hd_t *handle = hmac_context;

  g_assert (CYGNAL_IS_SESSION (user_data));

  gcry_md_write (*handle, data, data_len);

  return 0;
}

static gint
cygnal_session_hmac_sha256_final_cb (gpointer        hmac_context,
                                     signal_buffer **output,
                                     gpointer        user_data)
{
  gcry_md_hd_t *handle = hmac_context;
  signal_buffer *output_buffer;
  gcry_err_code_t err;

  g_assert (CYGNAL_IS_SESSION (user_data));

  if (!(output_buffer = signal_buffer_alloc (SHA256_DIGEST_LEN)))
    return SG_ERR_NOMEM;

  gcry_md_final (*handle);

  err = gcry_md_extract (*handle,
                         GCRY_MD_SHA256,
                         signal_buffer_data (output_buffer),
                         signal_buffer_len (output_buffer));

  if (err != GPG_ERR_NO_ERROR)
    {
      signal_buffer_free (output_buffer);
      return SG_ERR_NOMEM;
    }

  *output = g_steal_pointer (&output_buffer);

  return 0;
}

static void
cygnal_session_hmac_sha256_cleanup_cb (gpointer hmac_context,
                                       gpointer user_data)
{
  gcry_md_hd_t *handle = hmac_context;

  if (handle != NULL)
    {
      gcry_md_close (*handle);
      g_slice_free (gcry_md_hd_t, handle);
    }
}

static gint
cygnal_session_sha512_digest_init_cb (gpointer     *hmac_context,
                                      gpointer      user_data)
{
  CygnalSession *self = user_data;
  gcry_md_hd_t *handle;
  gcry_error_t err;

  g_assert (CYGNAL_IS_SESSION (self));

  handle = g_slice_new0 (gcry_md_hd_t);

  err = gcry_md_open (handle, GCRY_MD_SHA512, GCRY_MD_FLAG_SECURE);

  if (err != GPG_ERR_NO_ERROR)
    {
      g_slice_free (gcry_md_hd_t, handle);
      *hmac_context = NULL;
      return SG_ERR_NOMEM;
    }

  *hmac_context = g_steal_pointer (&handle);

  return 0;
}

static gint
cygnal_session_sha512_digest_update_cb (gpointer      hmac_context,
                                        const guint8 *data,
                                        gsize         data_len,
                                        gpointer      user_data)
{
  gcry_md_hd_t *handle = hmac_context;

  g_assert (hmac_context != NULL);
  g_assert (data != NULL);
  g_assert (CYGNAL_IS_SESSION (user_data));

  gcry_md_write (*handle, data, data_len);

  return 0;
}

static gint
cygnal_session_sha512_digest_final_cb (gpointer        hmac_context,
                                       signal_buffer **output,
                                       gpointer        user_data)
{
  gcry_md_hd_t *handle = hmac_context;
  signal_buffer *output_buffer = NULL;
  gcry_err_code_t err;

  g_assert (handle != NULL);
  g_assert (output != NULL);
  g_assert (CYGNAL_IS_SESSION (user_data));

  if (!(output_buffer = signal_buffer_alloc (SHA512_DIGEST_LEN)))
    return SG_ERR_NOMEM;

  gcry_md_final (*handle);

  err = gcry_md_extract (*handle,
                         GCRY_MD_SHA512,
                         signal_buffer_data (output_buffer),
                         signal_buffer_len (output_buffer));

  if (err != GPG_ERR_NO_ERROR)
    {
      signal_buffer_free (output_buffer);
      return SG_ERR_NOMEM;
    }

  *output = g_steal_pointer (&output_buffer);

  return 0;
}

static void
cygnal_session_sha512_digest_cleanup_cb (gpointer hmac_context,
                                         gpointer user_data)
{
  gcry_md_hd_t *handle = hmac_context;

  if (handle != NULL)
    {
      gcry_md_close (*handle);
      g_slice_free (gcry_md_hd_t, handle);
    }
}

static gint
cygnal_session_encrypt_cb (signal_buffer **output,
                           int             cipher,
                           const uint8_t  *key,
                           size_t          key_len,
                           const uint8_t  *iv,
                           size_t          iv_len,
                           const uint8_t  *plaintext,
                           size_t          plaintext_len,
                           gpointer        user_data)
{
  gcry_cipher_hd_t handle = NULL;
  signal_buffer *local_output = NULL;
  gcry_error_t err;
  int algo = -1;
  int mode = -1;

  g_assert (output != NULL);
  g_assert (CYGNAL_IS_SESSION (user_data));

  *output = NULL;

  switch (cipher)
    {
    case SG_CIPHER_AES_CBC_PKCS5:
      algo = GCRY_CIPHER_AES;
      mode = GCRY_CIPHER_MODE_CBC;
      break;

    case SG_CIPHER_AES_CTR_NOPADDING:
      algo = GCRY_CIPHER_AES;
      mode = GCRY_CIPHER_MODE_CTR;
      break;

    default:
      return SG_ERR_INVAL;
    }

  if (plaintext_len >= GLIB_SIZEOF_SSIZE_T)
    goto failure;

  err = gcry_cipher_open (&handle, algo, mode, GCRY_CIPHER_SECURE);
  if (err != GPG_ERR_NO_ERROR)
    goto failure;

  err = gcry_cipher_setkey (handle, key, key_len);
  if (err != GPG_ERR_NO_ERROR)
    goto failure;

  err = gcry_cipher_setiv (handle, iv, iv_len);
  if (err != GPG_ERR_NO_ERROR)
    goto failure;

  local_output = signal_buffer_alloc (next_power_of_two (plaintext_len));
  if (err != GPG_ERR_NO_ERROR)
    goto failure;

  err = gcry_cipher_encrypt (handle,
                             signal_buffer_data (local_output),
                             signal_buffer_len (local_output),
                             plaintext, plaintext_len);
  if (err != GPG_ERR_NO_ERROR)
    goto failure;

  *output = g_steal_pointer (&local_output);

  return 0;

failure:
  if (handle != NULL)
    gcry_cipher_close (handle);

  if (local_output != NULL)
    signal_buffer_free (local_output);

  return SG_ERR_NOMEM;
}

static void
cygnal_session_finalize (GObject *object)
{
  CygnalSession *self = (CygnalSession *)object;

  g_clear_pointer (&self->context, signal_context_destroy);

  g_rec_mutex_clear (&self->context_lock);

  G_OBJECT_CLASS (cygnal_session_parent_class)->finalize (object);
}

static void
cygnal_session_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
cygnal_session_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
cygnal_session_class_init (CygnalSessionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = cygnal_session_finalize;
  object_class->get_property = cygnal_session_get_property;
  object_class->set_property = cygnal_session_set_property;
}

static void
cygnal_session_init (CygnalSession *self)
{
  g_rec_mutex_init (&self->context_lock);
}

static void
cygnal_session_init_async (GAsyncInitable      *initable,
                           gint                 io_priority,
                           GCancellable        *cancellable,
                           GAsyncReadyCallback  callback,
                           gpointer             user_data)
{
  CygnalSession *self = (CygnalSession *)initable;
  g_autoptr(GTask) task = NULL;
  signal_crypto_provider crypto_provider = {
    .random_func = cygnal_session_random_generator_cb,
    .hmac_sha256_init_func = cygnal_session_hmac_sha256_init_cb,
    .hmac_sha256_update_func = cygnal_session_hmac_sha256_update_cb,
    .hmac_sha256_final_func = cygnal_session_hmac_sha256_final_cb,
    .hmac_sha256_cleanup_func = cygnal_session_hmac_sha256_cleanup_cb,
    .sha512_digest_init_func = cygnal_session_sha512_digest_init_cb,
    .sha512_digest_update_func = cygnal_session_sha512_digest_update_cb,
    .sha512_digest_final_func = cygnal_session_sha512_digest_final_cb,
    .sha512_digest_cleanup_func = cygnal_session_sha512_digest_cleanup_cb,
    .encrypt_func = cygnal_session_encrypt_cb,
#if 0
    .decrypt_func = cygnal_session_decrypt_cb,
#endif
    .user_data = self
  };

  g_assert (CYGNAL_IS_SESSION (initable));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, cygnal_session_init_async);
  g_task_set_priority (task, io_priority);

  signal_context_create (&self->context, self);
  signal_context_set_log_function (self->context, cygnal_session_log_cb);
  signal_context_set_locking_functions (self->context,
                                        cygnal_session_lock_cb,
                                        cygnal_session_unlock_cb);
  signal_context_set_crypto_provider (self->context, &crypto_provider);
}

static gboolean
cygnal_session_init_finish (GAsyncInitable  *initable,
                            GAsyncResult    *result,
                            GError         **error)
{
  g_assert (CYGNAL_IS_SESSION (initable));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (GAsyncInitableIface *iface)
{
  iface->init_async = cygnal_session_init_async;
  iface->init_finish = cygnal_session_init_finish;
}
